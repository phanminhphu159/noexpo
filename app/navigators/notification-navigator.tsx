/**
 * The app navigator (formerly "AppNavigator" and "MainNavigator") is used for the primary
 * navigation flows of your app.
 * Generally speaking, it will contain an auth flow (registration, login, forgot password)
 * and a "main" flow which the user will use once logged in.
 */
 import React from "react"
 import { createNativeStackNavigator } from "@react-navigation/native-stack"
 import { NotificationScreen,DemoListDetail} from "../screens"
 
 /**
  * This type allows TypeScript to know what routes are defined in this navigator
  * as well as what properties (if any) they might take when navigating to them.
  *
  * If no params are allowed, pass through `undefined`. Generally speaking, we
  * recommend using your MobX-State-Tree store(s) to keep application state
  * rather than passing state through navigation params.
  *
  * For more information, see this documentation:
  *   https://reactnavigation.org/docs/params/
  *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
  */
 export type NotificationNavigatorParamList = {
   notification_home: undefined
   demoListDetail: undefined
 }
 
 
 // Documentation: https://reactnavigation.org/docs/stack-navigator/
 // Stack Navigator
 const Stack = createNativeStackNavigator<NotificationNavigatorParamList>()
 
 export const Notification_Stack = () => {
   return (
     <Stack.Navigator
       screenOptions={{
         headerShown: false,
       }}
       initialRouteName="notification_home"
     >
       <Stack.Screen name="notification_home" component={NotificationScreen} />
      <Stack.Screen name="demoListDetail" component={DemoListDetail} />
     </Stack.Navigator>
   )
 }
 
 /**
  * A list of routes from which we're allowed to leave the app when
  * the user presses the back button on Android.
  *
  * Anything not on this list will be a standard `back` action in
  * react-navigation.
  *
  * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
  */
//   const exitRoutes = ["welcome"]
//  export const canExit = (routeName: string) => exitRoutes.includes(routeName)
 