import React, { useEffect, useRef } from 'react'
import { NotificationScreen, MessageScreen } from "../screens"
import { useColorScheme} from "react-native"
import { StyleSheet,TouchableOpacity} from "react-native"
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { navigationRef, useBackButtonHandler } from "./navigation-utilities"
import { NavigationContainer, DefaultTheme, DarkTheme } from "@react-navigation/native"
import {AppStack, Account_Stack,canExit} from "./index";
import Icon_animation,{Icons_animation} from "../components/icon/icon.animation";
import * as Animatable from 'react-native-animatable';

// Bottom tab Navigator
// style
const styles = StyleSheet.create({
    tabbar:{
        height: 60,
        position: 'absolute',
        bottom: 16,
        right: 16,
        left: 16,
        borderRadius: 16
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})
  
// ParamList
export type FooterNavigatorParamList = {
    Home: undefined
    Notification: undefined
    Account: undefined
    Message: undefined
}

// Animation 
const TabArr = [
    { route: 'Home', label: 'Home', type: Icons_animation.Ionicons, activeIcon: 'home', inActiveIcon: 'home-outline', component: AppStack },
    { route: 'Notification', label: 'Notification', type: Icons_animation.Ionicons, activeIcon: 'ios-notifications-sharp', inActiveIcon: 'ios-notifications-outline', component: NotificationScreen },
    { route: 'Account', label: 'Account', type: Icons_animation.MaterialCommunityIcons, activeIcon: 'account-circle', inActiveIcon: 'account-circle-outline', component: Account_Stack },
    { route: 'Message', label: 'Message', type: Icons_animation.MaterialCommunityIcons, activeIcon: 'message-processing', inActiveIcon: 'message-processing-outline', component: MessageScreen },
];

const TabButton = (props) => {
    const { item, onPress, accessibilityState } = props;
    const focused = accessibilityState.selected;
    const viewRef = useRef(null);
  
    useEffect(() => {
      if (focused) {
        viewRef.current.animate({0: {scale: .5, rotate: '0deg'}, 1: {scale: 1.5, rotate: '360deg'}});
      } else {
        viewRef.current.animate({0: {scale: 1.5, rotate: '360deg'}, 1: {scale: 1, rotate: '0deg'}});
      }
    }, [focused])
  
    return (
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={1}
        style={styles.container}>
        <Animatable.View
          ref={viewRef}
          duration={1000}
          style={styles.container}>
          <Icon_animation type={item.type} name={focused ? item.activeIcon : item.inActiveIcon} color={focused ? '#e32f45' : '#748c94'} size={22} style={{}} />
        </Animatable.View>
      </TouchableOpacity>
    )
  }
  
// Footer
const Tab = createBottomTabNavigator<FooterNavigatorParamList>()
const Bottom_Tabs = () => {
    return(
    <Tab.Navigator
        screenOptions={{
            headerShown: false,
            tabBarHideOnKeyboard: true,
            tabBarShowLabel: false,
            tabBarStyle: {
                ...styles.tabbar,
            },
        }}
        initialRouteName="Home"
    >
        
    {TabArr.map((item, index) => {  
      return (
        <Tab.Screen key={index} name={item.route} component={item.component}
          options={{
            tabBarButton: (props) => <TabButton {...props} item={item} />
          }}
        />
      )
    })}
  </Tab.Navigator>
  )
}


interface NavigationProps extends Partial<React.ComponentProps<typeof NavigationContainer>> {}
  
export const AppNavigator = (props: NavigationProps) => {
    const colorScheme = useColorScheme()
    useBackButtonHandler(canExit)
    return (
      <NavigationContainer
        ref={navigationRef}
        theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
        {...props}
      >
  
      <Bottom_Tabs />
      </NavigationContainer>
    )
  }
  
  AppNavigator.displayName = "AppNavigator"