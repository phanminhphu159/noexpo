import React, { FC, useEffect, useState } from "react"
import { View, ViewStyle, TextStyle,Image,ImageStyle, FlatList, TouchableHighlight, Animated } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { spacing } from "../../theme"
import { observer } from "mobx-react-lite"
import {
  Screen,
  Text,
  GradientBackground,
} from "../../components"
import Swipeable from 'react-native-gesture-handler/Swipeable'
import { color} from "../../theme"
import { NotificationNavigatorParamList } from "../../navigators"
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { useStores } from "../../models"
import {icons} from '../../components/icon/icons/index'
// import { array } from "mobx-state-tree/dist/internal"




const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
}
const TITLE: TextStyle = {
  fontSize: 24,
  fontWeight: 'bold',
  marginTop:10
}
const VIEW_HEADER: ViewStyle = {
  flexDirection: 'row',
  marginTop: 0,
  paddingHorizontal: 10,
  borderBottomWidth: 1,
  borderBottomColor: "#ffffff",
  alignItems: 'center',
}

const VIEW_BUTTON: ViewStyle = {
  flexDirection: 'row',
  marginTop: 0,
}
const BUTTON: ViewStyle = {
  marginLeft:15,
  flexDirection: 'row',
  borderRadius: 20,
  marginTop: 15,
  paddingHorizontal: 12,
  paddingVertical: 2,
  backgroundColor: "#5D2555"
}
const TEXT_BUTTON: TextStyle = {
  fontSize: 16,
}

const VIEW_NOTIFICATION: ViewStyle = {
  flexDirection: 'row',
  marginTop: 30,
}
const TEXT_NOTIFICATION: TextStyle = {
  paddingHorizontal: 15,
  fontSize: 16,
  fontWeight: 'bold',
}


const LIST_CONTAINER: ViewStyle = {
  alignItems: "center",
  flexDirection: "row",
  padding: 10
}
const FLAT_LIST: ViewStyle = {
  paddingHorizontal: spacing[4],
}
const IMAGE: ImageStyle = {
  borderRadius: 35,
  height: 65,
  width: 65,
}
const LIST_TEXT: TextStyle = {
  marginLeft: 10,
}
const IMAGE_ICON: ImageStyle = {
  width: 28,
  height: 28,
  tintColor: "#ffffff"
}

const VIEW_DELETE: ViewStyle = {
  backgroundColor: '#FF666C',
  alignItems: 'center',
  justifyContent: 'center',
  width: 70,
  height: 70,
  borderRadius: 15
}
  

export const NotificationScreen: FC<StackScreenProps<NotificationNavigatorParamList, "notification_home">> = observer(
  ({ navigation }) => {

    const { characterStore } = useStores() // lấy item == character
    const { characters } = characterStore
    const [list,setList] = useState(null);
    let row = []
    let prevOpenedRow;

    // Fecth Data
    useEffect(() => {
      async function fetchData() {
        await characterStore.getCharacters()
        await setList(characters);
      }
      fetchData()
    }, [])


    const ListDetail = (item) => navigation.navigate("demoListDetail",item)
    const RightSwipe = (dragX,index) =>{
      const scale = dragX.interpolate({
        inputRange: [0, 100],
        outputRange: [1,0],
        extrapolate: 'clamp'
      })
      return(
        <TouchableHighlight onPress={() => {HandleDelete(index)}} activeOpacity={0.6} style={{justifyContent: 'center',width: 70,height: 70,borderRadius: 15}}>
          <View style={VIEW_DELETE}>
            <Image style={IMAGE_ICON} resizeMode="cover" source={icons.delete} />
            <Animated.Text style={{transform: [{scale:scale}] , color: "#FFFFFF"}}>Delete</Animated.Text>
          </View>
        </TouchableHighlight>
      )
    }
    
    const HandleDelete = (index) => {
      const arr = [...list];
      arr.splice(index,1);
      setList(arr);
    }
    const closeRow = (index) => {
      if(prevOpenedRow && prevOpenedRow !== row[index]){
        prevOpenedRow.close();
      }
      prevOpenedRow = row[index];
    }


    return (
      <View testID="AcountScreen" style={FULL} >
        <GradientBackground colors={["#422443", "#281b34"]} />
        <Screen style={CONTAINER} backgroundColor={color.transparent} >
          
          {/* Header Design */}
          <View style={VIEW_HEADER}>
            <View style={{marginLeft: 10}}>
              <Text style={TITLE}>Notification</Text>
            </View>
              <Icon name="dots-horizontal" color="#FFFFFF" size={25} style={{ marginLeft:'auto'}}/>
          </View>

          {/* Button */}
          <View style={VIEW_BUTTON}>
            <View style={{...BUTTON}}>
              <Text style={TEXT_BUTTON}>All</Text>
            </View>
            <View style={BUTTON}>
              <Text style={TEXT_BUTTON}>Unread</Text>
            </View>
          </View>
          
          {/* New notification */}
          <View style={{...VIEW_NOTIFICATION}}>
            <Text style={{...TEXT_NOTIFICATION, fontSize: 17}}>New</Text>
            <TouchableHighlight onPress={() => {}} style={{marginLeft:'auto', borderRadius: 5}}>
              <Text style={{ paddingHorizontal: 15, color: "skyblue"}}>See all</Text>
            </TouchableHighlight>
          </View>
          
          {/* Flat List */}
          <View style={{paddingTop: 10, paddingBottom: 260}}>
          {list && (
          <FlatList
            contentContainerStyle={FLAT_LIST}
            data={list}
            keyExtractor={(item) => String(item.id)}
            

            renderItem={({ item, index }) => {
              return(
              <Swipeable 
                renderRightActions={(progress,dragX) => RightSwipe(dragX,index)}
                onSwipeableOpen={() => closeRow(index)}
                ref = {(ref) => row[index] = ref}
              >
                <TouchableHighlight 
                  activeOpacity={0.6}
                  onPress={() => {ListDetail(item)}} 
                  style={{borderRadius: 10}} 
                  underlayColor="#5D2555" 
                  > 
                  <View style={{...LIST_CONTAINER}}>
                    <Image source={{ uri: item.image }} style={IMAGE} />
                    <Text style={LIST_TEXT}>
                      {item.name} ({item.status})
                    </Text>
                  </View>
                </TouchableHighlight>
              </Swipeable>
              )
            }}

          />
          )}
          </View>
        </Screen>
      </View>
    )
  },
)
