export const icons = {
  back: require("./arrow-left.png"),
  bullet: require("./bullet.png"),
  bug: require("./ladybug.png"),
  home: require("./home.png"),
  notification: require("./notification.png"),
  account: require("./account.png"),
  message: require("./message.png"),
  email: require("./email.png"),
  card: require("./card.png"),
  heart: require("./heart.png"),
  phone: require("./phone.png"),
  settings: require("./settings.png"),
  share: require("./share.png"),
  support: require("./support.png"),
  place: require("./place.png"),
  edit_account: require("./edit_account.png"),
  double_right: require("./double_right.png"),
  delete: require("./delete.png"),
}

export type IconTypes = keyof typeof icons
