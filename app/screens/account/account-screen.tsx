import React, { FC } from "react"
import { View, ViewStyle, TextStyle,Image, ImageStyle, TouchableHighlight } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import {
  Screen,
  Text,
  GradientBackground,
} from "../../components"
import { color} from "../../theme"
import { AccountNavigatorParamList } from "../../navigators"
import {icons} from '../../components/icon/icons/index'

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
}
const TITLE: TextStyle = {
  fontSize: 24,
  fontWeight: 'bold',
  marginTop:10
}
const CAPTION: TextStyle = {
  fontSize: 14,
  lineHeight: 14,
  fontWeight: '500',
  marginTop:5
}
const VIEW_AVATAR: ViewStyle = {
  flexDirection: 'row',
  marginTop: 25,
  paddingHorizontal: 10
}
const AVATAR: ImageStyle = {
  width: 80,
  height: 80,
  borderRadius: 75,
  borderWidth:2,
}
const VIEW_ICON: ViewStyle = {
  marginLeft:10,
  flexDirection: 'row',
  marginTop: 10,
  paddingHorizontal: 20
}
const IMAGE_ICON1: ImageStyle = {
  width: 20,
  height: 20,
  tintColor:"#FFFFFF"
}
const IMAGE_ICON2: ImageStyle = {
  width: 20,
  height: 20,
  tintColor:"#FF6347" ,
  alignSelf: 'center'
}
const TEXT_ICON: TextStyle = {
  marginLeft: 10,
  fontSize: 14
}
const VIEW_TOUCHABLE_ITEM: ViewStyle = {
}
const VIEW_ITEM: ViewStyle = {
  flexDirection: 'row',
  paddingVertical: 12,
  paddingHorizontal: 20,
  borderBottomWidth:1,
  borderBottomColor: "#ffffff",
}
const MENU_ITEM_TEXT: TextStyle = {
  marginLeft: 20,
  fontWeight: '600',
  fontSize: 16,
  lineHeight: 26,
  alignSelf: 'center'
}


export const AcountScreen: FC<StackScreenProps<AccountNavigatorParamList, "account_home">> = observer(
  ({ navigation }) => {
  
    return (
      <View testID="AcountScreen" style={FULL} >
        <GradientBackground colors={["#422443", "#281b34"]} />
        <Screen style={CONTAINER} preset="scroll" backgroundColor={color.transparent}>
          
          {/* Header gonna be here */}
          {/* <Text style={TITLE_WRAPPER}>
            <Text style={TITLE} text="Header need here" />
          </Text> */}

          {/* Avatar */}
          <View style={VIEW_AVATAR}>
            <Image style={AVATAR} resizeMode="cover" source={require('../account/avatar.png')} />
            <View style={{marginLeft: 20}}>
              <Text style={TITLE}>Phan Minh Phú</Text>
              <Text style={CAPTION}>@phanminhphu159</Text>
            </View>
            <Image style={{width: 25,height: 25,tintColor:"#FF6347", marginLeft: 'auto' }} resizeMode="cover" source={icons.edit_account} />
          </View>

          {/* Some infomation to contact */}
          <View style={{...VIEW_ICON, marginTop: 15}}>
            <Image style={IMAGE_ICON1} resizeMode="cover" source={icons.place} />
            <Text style={TEXT_ICON}>Đà Nẵng, Việt Nam</Text>
          </View>
          <View style={VIEW_ICON}>
            <Image style={IMAGE_ICON1} resizeMode="cover" source={icons.phone} />
            <Text style={TEXT_ICON}>0905693609</Text>
          </View>
          <View style={VIEW_ICON}>
            <Image style={IMAGE_ICON1} resizeMode="cover" source={icons.email} />
            <Text style={TEXT_ICON}>phanminhphu159@gmail.com</Text>
          </View>
          
          {/* Touchable Icon + Text to see details */}
          <View style={{marginTop:100}}/>
          <TouchableHighlight style={{ borderTopWidth:1, borderTopColor: "#ffffff",}} onPress={() => {}}>
            <View style={VIEW_ITEM}>
              <Image style={IMAGE_ICON2} resizeMode="cover" source={icons.heart} />
              <Text style={MENU_ITEM_TEXT}>Your Favorites</Text>
              <Image style={{...IMAGE_ICON2, marginLeft: 'auto'}} resizeMode="cover" source={icons.double_right} />
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={VIEW_TOUCHABLE_ITEM} onPress={() => {}}>
            <View style={VIEW_ITEM}>
              <Image style={IMAGE_ICON2} resizeMode="cover" source={icons.card} />
              <Text style={MENU_ITEM_TEXT}>Payment</Text>
              <Image style={{...IMAGE_ICON2, marginLeft: 'auto'}} resizeMode="cover" source={icons.double_right} />
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={VIEW_TOUCHABLE_ITEM} onPress={() => {}}>
            <View style={VIEW_ITEM}>
              <Image style={IMAGE_ICON2} resizeMode="cover" source={icons.share} />
              <Text style={MENU_ITEM_TEXT}>Tell Your Friends</Text>
              <Image style={{...IMAGE_ICON2, marginLeft: 'auto'}} resizeMode="cover" source={icons.double_right} />
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={VIEW_TOUCHABLE_ITEM} onPress={() => {}}>
            <View style={VIEW_ITEM}>
              <Image style={IMAGE_ICON2} resizeMode="cover" source={icons.support} />
              <Text style={MENU_ITEM_TEXT}>Support</Text>
              <Image style={{...IMAGE_ICON2, marginLeft: 'auto'}} resizeMode="cover" source={icons.double_right} />
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={VIEW_TOUCHABLE_ITEM} onPress={() => {}}>
            <View style={VIEW_ITEM}>
              <Image style={IMAGE_ICON2} resizeMode="cover" source={icons.settings} />
              <Text style={MENU_ITEM_TEXT}>Settings</Text>
              <Image style={{...IMAGE_ICON2, marginLeft: 'auto'}} resizeMode="cover" source={icons.double_right} />
            </View>
          </TouchableHighlight>
        </Screen>
      </View>
    )
  },
)