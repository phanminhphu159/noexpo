import React, { useEffect, useRef } from 'react'
import { MessageScreen } from "../screens"
import { useColorScheme} from "react-native"
import { StyleSheet,TouchableOpacity,View, Image} from "react-native"
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { navigationRef, useBackButtonHandler } from "./navigation-utilities"
import { NavigationContainer, DefaultTheme, DarkTheme } from "@react-navigation/native"
import {AppStack, Notification_Stack ,Account_Stack,canExit} from "./index";
import * as Animatable from 'react-native-animatable';
import {icons} from '../../app/components/icon/icons/index'
// import Icon_animation from '../components/icon/icon.animation';

// Bottom tab Navigator
// style
const styles = StyleSheet.create({
  tabbar:{
      height: 60,
      position: 'absolute',
      bottom: 16,
      right: 16,
      left: 16,
      borderRadius: 16
  },
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
  },
  btn: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 4,
    borderColor: '#ffffff',
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center'
  },
  circle: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e32f45',
    borderRadius: 25,
  },
  text: {
    fontSize: 10,
    textAlign: 'center',
    color: '#e32f45',
  },
  icon: {
    width: 24,
    height: 24,
  }
})
  
// ParamList
export type FooterNavigatorParamList = {
    Home: undefined
    Notification: undefined
    Account: undefined
    Message: undefined
}

// Animation 
const TabArr = [
  { route: 'Home', label: 'Home', type: Image, icon: icons.home , component: AppStack },
  { route: 'Notification', label: 'Notification', type: Image, icon: icons.notification, component: Notification_Stack },
  { route: 'Account', label: 'Account', type:Image, icon: icons.account, component: Account_Stack },
  { route: 'Message', label: 'Message', type: Image, icon: icons.message, component: MessageScreen },
];


const animate1 = { 0: { scale: .5, translateY: 7 }, .92: { translateY: -30 }, 1: { scale: 1.2, translateY: -18 } }
// const animate1 = { 0: { scale: .5, translateY: 7 }, 1: { scale: 1.2, translateY: -24 } }
const animate2 = { 0: { scale: 1.2, translateY: -24 }, 1: { scale: 1, translateY: 7 } }

const circle1 = { 0: { scale: 0 }, 0.3: { scale: .9 }, 0.5: { scale: .2 }, 0.8: { scale: .7 }, 1: { scale: 1 } }
// const circle1 = { 0: { scale: 0 }, 1: { scale: 1 } }
const circle2 = { 0: { scale: 1 }, 0.3: { scale: 0 }, 1: { scale: 0 } }

const TabButton = (props) => {
  const { item, onPress, accessibilityState } = props; 
  const focused = accessibilityState.selected;
  const viewRef = useRef(null);
  const circleRef = useRef(null);
  const textRef = useRef(null);

  useEffect(() => {
    if (focused) {
      viewRef.current.animate(animate1);
      circleRef.current.animate(circle1);
      textRef.current.transitionTo({ scale: 1 });
    } else {
      viewRef.current.animate(animate2);
      circleRef.current.animate(circle2);
      textRef.current.transitionTo({ scale: 0 });
    }
  }, [focused])

  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      style={styles.container}>
      <Animatable.View
        ref={viewRef}
        duration={1000}
        style={styles.container}>
        <View style={styles.btn}>
          <Animatable.View
            ref={circleRef}
            style={styles.circle} />
          {/* <Icon_animation type={item.type} name={item.icon} color={focused ? '#ffffff' : '#748c94'} size={24} style={{}} /> */}
          <Image source={item.icon} style={{...styles.icon, tintColor: focused ? '#ffffff' : '#748c94'}}/>
        </View>
        <Animatable.Text
          ref={textRef}
          style={styles.text}>
          {item.label}
        </Animatable.Text>
      </Animatable.View>
    </TouchableOpacity>
  )
}
  
// Footer
const Tab = createBottomTabNavigator<FooterNavigatorParamList>()
const Bottom_Tabs = () => {
    return(
    <Tab.Navigator
        screenOptions={{
            headerShown: false,
            tabBarHideOnKeyboard: true,
            tabBarShowLabel: false,
            tabBarStyle: {
                ...styles.tabbar,
            },
        }}
        initialRouteName="Home"
    >
        
    {TabArr.map((item, index) => {
        return (
          <Tab.Screen key={index} name={item.route} component={item.component}
            options={{
              tabBarButton: (props) => <TabButton {...props} item={item} />
            }}
          />
        )
    })}
  </Tab.Navigator>
  )
}


interface NavigationProps extends Partial<React.ComponentProps<typeof NavigationContainer>> {}
  
export const AppNavigator = (props: NavigationProps) => {
    const colorScheme = useColorScheme()
    useBackButtonHandler(canExit)
    return (
      <NavigationContainer
        ref={navigationRef}
        theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
        {...props}
      >
  
      <Bottom_Tabs />
      </NavigationContainer>
    )
  }
  
  AppNavigator.displayName = "AppNavigator"